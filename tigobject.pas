{ <A Generic list type for tiOPF>

  Copyright (C) <2021> <Andrew Haines> <andrewd207@aol.com>

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version with the following modification:

  As a special exception, the copyright holders of this library give you
  permission to link this library with independent modules to produce an
  executable, regardless of the license terms of these independent modules,and
  to copy and distribute the resulting executable under terms of your choice,
  provided that you also meet, for each linked independent module, the terms
  and conditions of the license of that module. An independent module is a
  module which is not derived from or based on this library. If you modify
  this library, you may extend this exception to your version of the library,
  but you are not obligated to do so. If you do not wish to do so, delete this
  exception statement from your version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1335, USA.
}

{
  At some point I hope to add this to the official tiopf project.
  It implements a list type using generics.
}

unit tiGObject;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, tiObject, tiOID, tiFilteredObjectList;

type

  { TtiGObjectList }

  generic TtiGObjectList<T> = class(TtiFilteredObjectList)
  private
    function    GetItemsG(i: Integer): T;
    procedure   SetItemsG(i: Integer; AValue: T);
  public
    property    Items[i: Integer]: T read GetItemsG write SetItemsG;
    function    Find(AOIDToFindAsString : string): T;  override;
    function    Find(const AOIDToFind : TtiOID): TtiObject; override;
    function    Find(AOIDToFind : TtiOID; ASortType: TtiPerObjListSortType): TtiObject; reintroduce;
    function    FindInHierarchy(AOIDToFind : TtiOID): T;
    function    FindSortedUntyped(const AFindCompare: TtiObjectListFindCompareFunc; const AValue; out AObject: T; out AIndex: integer): boolean; reintroduce;
    function    Add(const AObject : T): integer;
    function    IndexOf(const AObject: T): integer;
    function    Last : T; override;
    function    First : T; override;
    function    FirstExcludeDeleted : T; override;
    function    LastExcludeDeleted : T; override;
    function    Remove(const AObject: T): integer; reintroduce;
    procedure   Extract(const AObject: T); reintroduce;
    procedure   Insert(const AIndex: integer; const AObject: T);
    procedure   Insert(const AInsertBefore: T; const AObject: T);
    function    FindByProps(const AProps : array of string;
                             const AVals : array of variant;
                             ACaseSensitive : boolean = true ): T; override;
  end;

implementation

{ TtiGObjectList }

function TtiGObjectList.GetItemsG(i: Integer): T;
begin
  result := T(inherited GetItems(i));
end;

procedure TtiGObjectList.SetItemsG(i: Integer; AValue: T);
begin
  SetItems(i, TtiObject(AValue));
end;

function TtiGObjectList.Find(AOIDToFindAsString: string): T;
begin
  Result:= T(inherited Find(AOIDToFindAsString));
end;

function TtiGObjectList.Find(const AOIDToFind: TtiOID): TtiObject;
begin
  Result:=T(inherited Find(AOIDToFind));
end;

function TtiGObjectList.Find(AOIDToFind: TtiOID;
  ASortType: TtiPerObjListSortType): TtiObject;
begin
  Result := T(inherited Find(AOIDToFind, ASortType));
end;

function TtiGObjectList.FindInHierarchy(AOIDToFind: TtiOID): T;
begin
  result := T(inherited FindInHierarchy(AOIDToFind));
end;

function TtiGObjectList.FindSortedUntyped(
  const AFindCompare: TtiObjectListFindCompareFunc; const AValue; out
  AObject: T; out AIndex: integer): boolean;
var
  tmp: TtiObject;
begin
  Result := inherited FindSortedUntyped(AFindCompare, AValue, tmp, AIndex);
  AObject:= T(tmp);
end;

function TtiGObjectList.Add(const AObject: T): integer;
begin
  Result:=inherited Add(AObject);
end;

function TtiGObjectList.IndexOf(const AObject: T): integer;
begin
  Result:=inherited IndexOf(TtiObject(AObject));
end;

function TtiGObjectList.Last: T;
begin
  Result:= T(inherited Last);
end;

function TtiGObjectList.First: T;
begin
  Result:=T(inherited First);
end;

function TtiGObjectList.FirstExcludeDeleted: T;
begin
  Result:=T(inherited FirstExcludeDeleted);
end;

function TtiGObjectList.LastExcludeDeleted: T;
begin
  Result:=T(inherited LastExcludeDeleted);
end;

function TtiGObjectList.Remove(const AObject: T): integer;
begin
  Result:=inherited Remove(AObject);
end;

procedure TtiGObjectList.Extract(const AObject: T);
begin
  inherited Extract(AObject)
end;

procedure TtiGObjectList.Insert(const AIndex: integer; const AObject: T);
begin
  inherited Insert(AIndex, AObject);
end;

procedure TtiGObjectList.Insert(const AInsertBefore: T; const AObject: T);
begin
  inherited Insert(TtiObject(AInsertBefore), TtiObject(AObject));
end;

function TtiGObjectList.FindByProps(const AProps: array of string;
  const AVals: array of variant; ACaseSensitive: boolean): T;
begin
  Result:=T(inherited FindByProps(AProps, AVals, ACaseSensitive));
end;

end.

