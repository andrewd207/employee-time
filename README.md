# Employee time

A simple program to track the worktime of employee's that call or text their time.

It's created using sqlite3 and [tiOPF](https://github.com/graemeg/tiopf)

To create the innosetup you need to get the sqlite dll.

[Sqlite](https://www.sqlite.org/index.html)
