{ <Main form>

  Copyright (C) <2021> <Andrew Haines> <andrewd207@aol.com>

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}
unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls, ExtDlgs, Menus, IpHtml, XMLConf, tiListMediators, bom_objects;


type

  { TForm1 }

  TForm1 = class(TForm)
    CalendarDialog1: TCalendarDialog;
    AutoMarkCheckBox: TCheckBox;
    MenuItemCopy: TMenuItem;
    MenuItemReportDelete: TMenuItem;
    ReportPopup: TPopupMenu;
    ReportListBox: TListBox;
    UnmarkButton: TButton;
    MarkButton: TButton;
    GenerateButton: TButton;
    Memo1: TMemo;
    PageControl1: TPageControl;
    Employees: TTabSheet;
    ReportSheet: TTabSheet;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure GenerateButtonClick(Sender: TObject);
    procedure MarkButtonClick(Sender: TObject);
    procedure MenuItemCopyClick(Sender: TObject);
    procedure MenuItemReportDeleteClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ReportListBoxSelectionChange(Sender: TObject; User: boolean);
    procedure ReportPopupPopup(Sender: TObject);
    procedure UnmarkButtonClick(Sender: TObject);
  private
    FDefaultMemoText: String;
    FObserver: Tobject;
    FXMLCfg: TXMLConfig;
    FReportMediator: TtiListBoxListMediatorView;
    function GetSelectedReport: TReportItem;
    procedure SaveConfig;
    procedure LoadConfig;
  public

  end;

var
  Form1: TForm1;

implementation
uses
  frame_employee, frame_Worktimes, tiObject, DateUtils, Clipbrd, tiLog;

type

  { TEmployeeObserver }

  TEmployeeObserver = class(TtiObject)
    procedure Update(ASubject: TtiObject; AOperation: TNotifyOperation; AData: TtiObject); overload; override;
  end;

{$R *.lfm}

procedure TEmployeeObserver.Update(ASubject: TtiObject;
  AOperation: TNotifyOperation; AData: TtiObject);
var
  lNewTab: TTabSheet;
  lItem: TEmployee absolute AData;
  lFrame: TWorktimesframe;
  lCompName: String;

  function FindTabSheet(AItem: TtiObject): TTabSheet;
  var
    lTag: PtrInt;
    i: Integer;
  begin
    Result := nil;
    lTag:= PtrInt(Pointer(AItem));;
    for i := 0 to Form1.PageControl1.PageCount-1 do
      if Form1.PageControl1.Pages[i].Tag = lTag then
        Exit(Form1.PageControl1.Pages[i]);
  end;

begin
  inherited Update(ASubject, AOperation, AData);

  case AOperation of
    noAddItem:
    begin
      lItem.AttachObserver(Self);
      if not lItem.Active then
        Exit;
      GLog.Log(ASubject.ClassName + ' ' + AData.ClassName);
      lCompName := '_'+StringReplace(lItem.OID.AsString, '-', '_', [rfReplaceAll]);

      lNewTab := Form1.PageControl1.AddTabSheet;
      lNewTab.PageIndex:=lItem.Index+2;
      lNewTab.Caption:= lItem.Name;
      lNewTab.Tag:=Ptrint(Pointer(lItem));
      lNewTab.Name:='';
      lFrame := TWorktimesframe.Create(lNewTab);
      lFrame.Name:='_';
      lFrame.Parent := lNewTab;
      lFrame.Align:= alClient;
      lFrame.Tag:=Ptrint(Pointer(lItem));
      lFrame.SetupMediator;
    end;
    noChanged:
    begin
      if ASubject is TEmployeeList then
      begin
        GLog.Log('EmployeeList Changes');
      end
      else
      begin
        lItem := ASubject as TEmployee;
        lCompName := '_'+StringReplace(lItem.OID.AsString, '-', '_', [rfReplaceAll]);
        lNewTab := FindTabSheet(lItem);
        if lNewTab = nil then
          Update(ASubject, noAddItem, AData)
        else
        begin
          lNewTab.Caption:=lItem.Name;
          if not lItem.Active then
          begin
            lNewTab.Free;
          end;
        end;
      end;
    end;
    noDeleteItem:
    begin
      //lCompName := '_'+StringReplace(lItem.OID.AsString, '-', '_', [rfReplaceAll]);
      lNEwTAb := FindTabSheet(lItem);
      //lNewTab := TTabSheet(Form1.PageControl1.FindComponent(lCompName));
      lNewTab.Free;

    end;
  end;


end;

{ TEmployeeObserver }


{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
  lFrame: TFrame1;
  lObserver: TEmployeeObserver;
  lMediator: TtiListBoxListMediatorView;
begin
  FXMLCfg := TXMLConfig.Create(Self);
  LoadConfig;
  lFrame := TFrame1.Create(Employees);
  lFrame.Parent := Employees;
  lFRame.Align:=alClient;
  lFrame.InitMediator;

  lObserver := TEmployeeObserver.Create;
  FObserver := lObserver;

  GEmployees.AttachObserver(lObserver);
  GEmployees.Read;

  Memo1.Lines.Add('');
  Memo1.Lines.Add('All data saved in ' + GetSaveFile);
  FDefaultMemoText:=Memo1.Text;

  FReportMediator := TtiListBoxListMediatorView.Create;
  FReportMediator.SetView(ReportListBox);
  FReportMediator.FieldName:='DateGenerated';
  FReportMediator.Subject := GReports;
  FReportMediator.Active:=True;
end;

function MyDaysBetween(AFirst, ALast: TDateTime): Integer;
begin
  Result := 0;
  while AFirst < ALast do
  begin
    Inc(Result);
    AFirst:=IncDay(AFirst);
  end;

end;

procedure TForm1.GenerateButtonClick(Sender: TObject);
var
  lReport: TReportItem;
  lDays: Integer;
begin
  lReport := GReports.GenerateReportForAll(False, False);
  lDays := DaysBetween(lReport.StartDate, lReport.EndDate)+1;

  Memo1.Lines.Text := (lReport.Text);
  Clipboard.AsText := lReport.Text;

  if AutoMarkCheckBox.Checked then
    GEmployees.MarkAllSubmitted;

  if lDays > 14 then
  begin
    ShowMessage(Format('This report has more than 14 days [%d]'+LineEnding+
                             'Be sure to mark old entries as submitted', [lDays]));
  end;
  ReportListBox.Selected[0] := True;

end;

procedure TForm1.MarkButtonClick(Sender: TObject);
begin
  GEmployees.MarkAllSubmitted;
end;

procedure TForm1.MenuItemCopyClick(Sender: TObject);
var
  lReport: TReportItem;
begin
  lReport := GetSelectedReport;
  if Assigned(lReport) then
  begin
    Clipboard.AsText := lReport.Text;
  end;
end;

procedure TForm1.MenuItemReportDeleteClick(Sender: TObject);
var
  lReport: TReportItem;
begin
  lReport := GetSelectedReport;
  if Assigned(lReport) then
  begin
    if  QuestionDlg('Delete report', 'Delete report ' + DateTimeToStr(lReport.DateGenerated) + ' permanantly?',
        TMsgDlgType.mtWarning, [mrYes, 'Yes', mrCancel, 'Cancel'], 0) = mrYes then
    begin
      lReport.Deleted:=True;
      lReport.Save;
      GReports.Remove(lReport);
    end;
  end;
end;

procedure TForm1.PageControl1Change(Sender: TObject);
var
  lFrame: TWorktimesframe;
begin
  Caption:= ' Employee Time - '+ PageControl1.ActivePage.Caption;
  {lFrame := TWorktimesframe(PageControl1.ActivePage.FindChildControl('_'));
  if Assigned(lFrame) and (lFrame is TWorktimesframe)then
    lFrame.SetupMediator;}
end;

procedure TForm1.ReportListBoxSelectionChange(Sender: TObject; User: boolean);
var
  lReport: TReportItem;
  i: Integer;
begin
   lReport := GetSelectedReport;
   if Assigned(lReport) then
   begin
     Memo1.Text:=lReport.Text;
   end
   else
   begin
     Memo1.Text := FDefaultMemoText
   end;
end;

procedure TForm1.ReportPopupPopup(Sender: TObject);
begin
  MenuItemCopy.Enabled:=GetSelectedReport <> nil;
  MenuItemReportDelete.Enabled:=GetSelectedReport <> nil;
end;

procedure TForm1.UnmarkButtonClick(Sender: TObject);
begin
  if CalendarDialog1.Execute then
  begin
    GEmployees.UnMarkToDate(CalendarDialog1.Date);
  end;
end;

function TForm1.GetSelectedReport: TReportItem;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to ReportListBox.Count-1 do
  begin
    if ReportListBox.Selected[i] then
    begin
      Result := TReportItem(FReportMediator.GetObjectFromRow(i));
      Exit;
    end;
  end;

end;

procedure TForm1.SaveConfig;
begin
  FXMLCfg.SetValue('window/position/top', Top);
  FXMLCfg.SetValue('window/position/left', Left);
  FXMLCfg.SetValue('window/position/width', Width);
  FXMLCfg.SetValue('window/position/left', Height);
  FXMLCfg.SetValue('window/position/state', Ord(WindowState));
  FXMLCfg.SetDeleteValue('settings/generate/automark', AutoMarkCheckBox.Checked, False);

end;

procedure TForm1.LoadConfig;
begin
  FXMLCfg.Filename:=IncludeTrailingPathDelimiter(GetAppConfigDir(False))+'config.xml';
  Top := FXMLCfg.GetValue('window/position/top', Top);
  Left := FXMLCfg.GetValue('window/position/left', Left);
  Width := FXMLCfg.GetValue('window/position/width', Width);
  Height := FXMLCfg.GetValue('window/position/left', Height);
  WindowState:= TWindowState(FXMLCfg.GetValue('window/position/state', Ord(WindowState)));
  AutoMarkCheckBox.Checked := FXMLCfg.GetValue('settings/generate/automark', False);

end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  GEmployees.save;
  SaveConfig;
end;

end.

