{ <objects used to acces the database with tiOPF>

  Copyright (C) <2021> <Andrew Haines> <andrewd207@aol.com>

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit bom_objects;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, tiGObject, tiObject,tiQuerySqldbSQLite3, tiConstants, tiAutoMap, tiQuery;

type
  TTableItemClass = class of TTableItem;

  { TTableItem }

  TTableItem = class(TTiObject)
    class function GetTable(AVersion: Integer; AIgnoreBefore: Integer): TtiDBMetaDataTable; virtual; abstract;
    class function GetTableVersion: Integer; virtual; abstract;
    class function GetTableName: String; virtual; abstract;
    class procedure RegisterMappings; virtual; abstract;
  end;

  TWorkList = class;
  { TEmployee }

  TEmployee = class(TTableItem)
  private const
    cVersion = 2;
    cTableName = 'employees';
    class function GetTable(AVersion: Integer; AIgnoreBefore: Integer): TtiDBMetaDataTable; override;
    class procedure RegisterMappings; override;
    class function GetTableVersion: Integer; override;
    class function GetTableName: String; override;
  private
    FActive: Boolean;
    FIsDeleted: Boolean;
    FName: String;
    FWorkList: TWorkList;
    function GetWorkList: TWorkList;
    procedure SetActive(AValue: Boolean);
    procedure SetIsDeleted(AValue: Boolean);
    procedure SetName(AValue: String);
  public
    procedure GenerateDetails(ALines: TStrings; var AFirst: TDateTime;
      var ALast: TDateTime; AIncludeSubmitted: Boolean; AsHTML: Boolean);
    property WorkList: TWorkList read GetWorkList;
  published
    property Name: String read FName write SetName;
    property Active: Boolean read FActive write SetActive;
    property IsDeleted: Boolean read FIsDeleted write SetIsDeleted;
  public
    destructor Destroy; override;
  end;


  { TEmployeeList }

  TEmployeeList = class (specialize TtiGObjectList<TEmployee>)
  private
    FShowDeleted: Boolean;
    procedure SetShowDeleted(AValue: Boolean);
  public
    procedure Read; overload; override;
    procedure Save; overload; override;
    procedure UnMarkToDate(ADate: TDate);
    procedure MarkAllSubmitted;
    property ShowDeleted: Boolean read FShowDeleted write SetShowDeleted;
  end;

  { TWorkEntry }

  TWorkEntry = class(TTAbleItem)
  private const
    cVersion = 3;
    cTableName = 'workentries';
    class function GetTable(AVersion: Integer; AIgnoreBefore: Integer): TtiDBMetaDataTable; override;
    class procedure RegisterMappings; override;
    class function GetTableVersion: Integer; override;
    class function GetTableName: String; override;
  private
    FDate: TDateTime;
    FEmployee: TEmployee;
    FEndTime: TTime;
    FStartTime: TTime;
    FSubmitted: Boolean;
    function GetEmployeeOID: String;
    function GetHours: Double;
    procedure SetDate(AValue: TDateTime);
    procedure SetEmployee(AValue: TEmployee);
    procedure SetEmployeeOID(AValue: String);
    procedure SetEndTime(AValue: TTime);
    procedure SetStartTime(AValue: TTime);
    procedure SetSubmitted(AValue: Boolean);
  public
    property Employee: TEmployee read FEmployee write SetEmployee;
    procedure Save; overload; override;
  published
    property EmployeeOID: String read GetEmployeeOID write SetEmployeeOID;

    property Date: TDateTime read FDate write SetDate;
    property StartTime: TTime read FStartTime write SetStartTime;
    property EndTime: TTime read FEndTime write SetEndTime;
    property Submitted: Boolean read FSubmitted write SetSubmitted;
    // published but not stored in the database. calculated
    property Hours: Double read GetHours;
  end;

  { TReportItem }

  TReportItem = class(TTableItem)
  private const
    cVersion = 1;
    cTableName = 'reports';
    class function GetTable(AVersion: Integer; AIgnoreBefore: Integer): TtiDBMetaDataTable; override;
    class procedure RegisterMappings; override;
    class function GetTableVersion: Integer; override;
    class function GetTableName: String; override;
  private
    FDateGenerated: TDateTime;
    FEndDate: TDateTime;
    FStartDate: TDateTime;
    FText: String;
    procedure SetDateGenerated(AValue: TDateTime);
    procedure SetEndDate(AValue: TDateTime);
    procedure SetStartDate(AValue: TDateTime);
    procedure SetText(AValue: String);
  public

    procedure Save; overload; override;
    procedure Read; overload; override;
  published
    property Text: String read FText write SetText;
    property StartDate: TDateTime read FStartDate write SetStartDate;
    property EndDate: TDateTime read FEndDate write SetEndDate;
    property DateGenerated: TDateTime read FDateGenerated write SetDateGenerated;
  end;

    { TReportList }

    TReportList = class(specialize TtiGObjectList<TReportItem>)
    public
      function GenerateReportForAll(AIncludeSubmitted: Boolean; AsHtml: Boolean): TReportItem;
      procedure Save; overload; override;
      procedure Read; overload; override;
    end;

  { TWorkList }

  TWorkList = class(specialize TtiGObjectList<TWorkEntry>)
  private
    FEmployee: TEmployee;
    FShowSubmitted: Boolean;
    procedure SetShowSubmitted(AValue: Boolean);
  public
    procedure Save; overload; override;
    procedure Read; overload; override;
    function Hours(AIncludeSubmitted: Boolean): Double;
    procedure UnMarkToDate(ADate: TDate);
    procedure MarkAllSubmitted;
  published
    constructor Create(AnEmployee: TEmployee);
    property Employee: TEmployee read FEmployee;
    property ShowSubmitted: Boolean read FShowSubmitted write SetShowSubmitted;
  end;

  { TTableVersionEntry }

  TTableVersionEntry = class(TtiObject)
  private const
    cVersion = 1;
    cTableName = 'tableversion';
    class procedure RegisterMappings;
  private
    FName: String;
    FVersion: Integer;
    procedure SetName(AValue: String);
    procedure SetVersion(AValue: Integer);
  published
    property Name: String read FName write SetName;
    property Version: Integer read FVersion write SetVersion;
  end;

  { TTableVersionList }

  TTableVersionList = class(specialize TtiGObjectList<TTableVersionEntry>)
    procedure CheckUpgrade;
    procedure CreateTables;
    constructor Create; override;
  end;



  function GEmployees: TEmployeeList;
  function GReports: TReportList;

  function GetSaveFile: String;

  function Pad(S: String; PadChar: Char = ' '; Count: Integer = 9): String;

implementation

uses
  tiOPFManager, tiMediators, tiListMediators, tiLog, tiLogToConsole, DateUtils, math;
var
  privEmployees: TEmployeeList;
  privReports: TReportList;
  GDBTables: TTableVersionList = nil;

function GEmployees: TEmployeeList;
begin
  if not Assigned(privEmployees) then
  begin
    privEmployees := TEmployeeList.Create;
  end;
  Result := privEmployees;
end;

function GReports: TReportList;
begin
  if not Assigned(privReports) then
  begin
    privReports := TReportList.Create;
    privReports.Read;
  end;
  Result := privReports;
end;

function GetSaveFile: String;
begin
  Result := IncludeTrailingPathDelimiter(GetAppConfigDir(False)) + 'data.sqlite';
end;

{ TReportList }

function TReportList.GenerateReportForAll(AIncludeSubmitted: Boolean;
  AsHtml: Boolean): TReportItem;
var
  lEmployee: TEmployee;
  i, lDays: Integer;
  lLines: TStrings;
  lFirstDate: TDate = 0;
  lLastDate: TDate = 0;

begin
  Result := TReportItem.CreateNew(Self);
  lLines := TStringList.Create;

  if AsHtml then
  begin
    lLines.Add('<style>');
    lLines.Add(' table{border-collapse:collapse; border:1px solid #0FFF00; } table td{ border:1px solid #FFFF00; }');
    lLines.Add('</style>');
    lLines.Add('<h2>Hours Summary</h2>');
    lLines.Add('<ul>');
    for i := 0 to GEmployees.Count-1 do
    begin
      lEmployee := GEmployees.Items[i];
      if not lEmployee.Active then
        Continue;
      lLines.Add('<li>%s: %.2f</li>', [lEmployee.Name, lEmployee.WorkList.Hours(AIncludeSubmitted)]);
    end;
    lLines.Add('</ul>');
    lLines.Add('<hr />');
  lLines.Add('<h1>Full Report</h1>');
  end
  else
  begin
    lLines.Add('## Hours Summary');
    for i := 0 to GEmployees.Count-1 do
    begin
      lEmployee := GEmployees.Items[i];
      if not lEmployee.Active then
        Continue;
      lLines.Add('- %s: %.2f', [lEmployee.Name, lEmployee.WorkList.Hours(AIncludeSubmitted)]);
    end;
    lLines.Add('---');
    lLines.Add('# Full Report');
  end;

  for i := 0 to GEmployees.Count-1 do
  begin
    lEmployee := GEmployees.Items[i];
    if not lEmployee.Active then
      Continue;

    lLines.add('');
    lEmployee.GenerateDetails(lLines, lFirstDate, lLastDate, AIncludeSubmitted, AsHtml);
  end;

  lDays := DaysBetween(lFirstDate, lLastDate)+1;

  if AsHtml then
  begin
    lLines.Insert(0, Format('<h1>Report for %s to %s [<b>%d days</b>]</h1>', [
                     FormatDateTime(DefaultFormatSettings.LongDateFormat, lFirstDate),
                     FormatDateTime(DefaultFormatSettings.LongDateFormat, lLastDate),
                     lDays                                       ]));

  end
  else
  begin
    lLines.Insert(0, Format('# Report for %s to %s [**%d days**]', [
                     FormatDateTime(DefaultFormatSettings.LongDateFormat, lFirstDate),
                     FormatDateTime(DefaultFormatSettings.LongDateFormat, lLastDate),
                     lDays
                     ]));
  end;




  Result.Text := lLines.Text;
  Result.DateGenerated:=Now;
  Result.StartDate:=lFirstDate;
  Result.EndDate:=lLastDate;
  lLines.Free;
  Insert(0, Result);
  REsult.Save;


end;

procedure TReportList.Save;
begin
  inherited Save;
end;

procedure TReportList.Read;
begin
  Criteria.ClearAll;
  Criteria.AddOrderBy('f_dategenerated', False);
  inherited Read;
end;

{ TReportItem }

class function TReportItem.GetTable(AVersion: Integer; AIgnoreBefore: Integer
  ): TtiDBMetaDataTable;
begin
  Result := TtiDBMetaDataTable.Create;
  Result.Name:=cTableName;
  if (AVersion >=1) and (AIgnoreBefore <1) then
  begin
    Result.AddInstance('f_oid', qfkString, 36);
    Result.AddInstance('f_dategenerated', qfkDateTime);
    Result.AddInstance('f_startdate', qfkDateTime);
    Result.AddInstance('f_enddate', qfkDateTime);
    Result.AddInstance('f_text', qfkLongString);
  end;
end;

class procedure TReportItem.RegisterMappings;
begin
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'oid', 'f_oid', [pktDB]);
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'DateGenerated', 'f_dategenerated');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'StartDate', 'f_startdate');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'EndDate', 'f_enddate');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'Text', 'f_text');
  GTIOPFManager.ClassDBMappingMgr.RegisterCollection(TReportList, Self);


end;

class function TReportItem.GetTableVersion: Integer;
begin
  Result := cVersion;
end;

class function TReportItem.GetTableName: String;
begin
  Result := cTableName;
end;

procedure TReportItem.SetDateGenerated(AValue: TDateTime);
begin
  if FDateGenerated=AValue then Exit;
  FDateGenerated:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then
    ObjectState:=posUpdate;
end;

procedure TReportItem.SetEndDate(AValue: TDateTime);
begin
  if FEndDate=AValue then Exit;
  FEndDate:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then
    ObjectState:=posUpdate;
end;

procedure TReportItem.SetStartDate(AValue: TDateTime);
begin
  if FStartDate=AValue then Exit;
  FStartDate:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then
    ObjectState:=posUpdate;
end;

procedure TReportItem.SetText(AValue: String);
begin
  if FText=AValue then Exit;
  FText:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then
    ObjectState:=posUpdate;
end;

procedure TReportItem.Save;
begin
  inherited Save;
end;

procedure TReportItem.Read;
begin
  inherited Read;
end;

{ TTableVersionEntry }

class procedure TTableVersionEntry.RegisterMappings;
begin
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'oid', 'f_oid', [pktDB]);
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'name', 'f_name');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'version', 'f_version');
  GTIOPFManager.ClassDBMappingMgr.RegisterCollection(TTableVersionList, Self);
end;

procedure TTableVersionEntry.SetName(AValue: String);
begin
  if FName=AValue then Exit;
  FName:=AValue;
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TTableVersionEntry.SetVersion(AValue: Integer);
begin
  if FVersion=AValue then Exit;
  FVersion:=AValue;
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

{ TTableVersionList }

type

    { TDBHack }

    TDBHack = class(TtiDatabaseSQLDBSQLite3)
      function FieldMetaDataToSQLCreate(const AFieldMetaData: TtiDBMetaDataField
        ): string; override;
    end;

{ TDBHack }

function TDBHack.FieldMetaDataToSQLCreate(
  const AFieldMetaData: TtiDBMetaDataField): string;
begin
  Result:=inherited FieldMetaDataToSQLCreate(AFieldMetaData);
end;

procedure TTableVersionList.CheckUpgrade;
var
  lEntry: TTableVersionEntry;
  procedure UpgradeTable(ATable: TtiDBMetaDataTable);
  var
    lSQL, lFieldType: String;
    i: Integer;
    lFieldName: TFieldName;
    lDBhack: TDBHack;

  begin
    lDBhack := TDBHack.Create;
    for i := 0 to ATable.Count-1 do
    begin
      lFieldName := ATable.Items[i].Name;
      lFieldType := lDBhack.FieldMetaDataToSQLCreate(ATable.Items[i]);
      lSQL := Format('ALTER TABLE %s ADD COLUMN  %s %s', [ATable.Name, lFieldName, lFieldType]);
      GTIOPFManager.ExecSQL(lSQL, 'db');
    end;
    lDBhack.Free;

  end;

  procedure CheckUpgradeTable(AClass: TTableItemClass);
  begin
    lEntry := FindByProps(['name'], [AClass.GetTableName]);
    if Assigned(lEntry) then
    begin
      if lEntry.Version <> AClass.GetTableVersion then
      begin
        UpgradeTable(AClass.GetTable(AClass.GetTableVersion, lEntry.Version));
        lEntry.Version:=AClass.GetTableVersion;
        lEntry.Save();
      end;
    end
    else
    begin
      lEntry := TTableVersionEntry.CreateNew();
      lEntry.Name:=AClass.GetTableName;
      lEntry.Version:=AClass.GetTableVersion;
      lEntry.Save;
    end;
  end;

begin

  // employee
  CheckUpgradeTable(TEmployee);
  // worktimes
  CheckUpgradeTable(TWorkEntry);
  // reports
  CheckUpgradeTable(TReportItem);
end;

procedure TTableVersionList.CreateTables;
  procedure CheckTable(AClass: TTableItemClass);
  var
    lTable: TtiDBMetaDataTable;
  begin
    if not GTIOPFManager.TableExists(AClass.GetTableName) then
    begin
      lTable := AClass.GetTable(AClass.GetTableVersion, 0);
      GTIOPFManager.CreateTable(lTable, 'db');
      lTable.Free;
    end;
  end;

begin
  CheckTable(TEmployee);
  CheckTable(TWorkEntry);
  CheckTable(TReportItem);
end;

constructor TTableVersionList.Create;
var
  lTable: TtiDBMetaDataTable;
  lItem: TTableVersionEntry;
begin
  inherited Create;
  Clear;
  if not GTIOPFManager.TableExists('tableversion') then
  begin
    lTable := TtiDBMetaDataTable.Create;
    lTable.Name:='tableversion';
    lTable.AddInstance('f_oid', qfkString, 36);
    lTable.AddInstance('f_name', qfkLongString);
    lTable.AddInstance('f_version', qfkInteger);
    GTIOPFManager.CreateTable(lTable, 'db');

    lItem := TTableVersionEntry.CreateNew();
    lItem.Name:='employees';
    lItem.Version := TEmployee.cVersion;
    Add(lItem);

    lItem := TTableVersionEntry.CreateNew();
    lItem.Name:='workentries';
    lItem.Version := TWorkEntry.cVersion;
    Add(lItem);
    Save;
  end
  else
    Read;

end;

{ TWorkList }

procedure TWorkList.SetShowSubmitted(AValue: Boolean);
begin
  if FShowSubmitted=AValue then Exit;
  FShowSubmitted:=AValue;
  BeginUpdate();
  Clear;
  Criteria.ClearAll;
  Criteria.AddEqualTo('f_employee', FEmployee.OID.AsString);
  Criteria.AddOrderBy('f_date');
  if AValue then
  begin
    Criteria.AddEqualTo('f_submitted', True);
  end;
  Read;
  EndUpdate;
end;

procedure TWorkList.Save;
begin
  inherited Save;
end;

procedure TWorkList.Read;
begin
  Criteria.ClearAll;
  Criteria.AddEqualTo('f_employee', Employee.OID.AsString);
  if not ShowSubmitted then
    Criteria.AddEqualTo('f_submitted', False);
  inherited Read;

end;

function TWorkList.Hours(AIncludeSubmitted: Boolean): Double;
var
  i: Integer;
  lItem: TWorkEntry;
  lTotal: Double = 0.0;
begin
  for i := 0 to Count-1 do
  begin
    lItem := Items[i];
    if lItem.Submitted and not AIncludeSubmitted then
      Continue;

    lTotal+=lItem.Hours;
  end;
  Result := lTotal;
end;

procedure TWorkList.UnMarkToDate(ADate: TDate);
var
  lSQL: String;
  lDate: String;
begin

  lDate := FormatFloat('0.0', DateTimeToJulianDate(ADate));
  lSQL := Format('UPDATE %s SET f_submitted = "F" where f_employee = "%s" AND f_date >= %s', [
       TWorkEntry.GetTableName,
       Employee.OID.AsString,
       lDate
       ]);
  GTIOPFManager.ExecSQL(lSQL, 'db');
  Clear;
  Read;
end;

procedure TWorkList.MarkAllSubmitted;
var
  lSQL: String;
begin
  lSQL := Format('UPDATE %s SET f_submitted = "T" where f_employee = "%s"', [
       TWorkEntry.GetTableName,
       Employee.OID.AsString
       ]);
  GTIOPFManager.ExecSQL(lSQL, 'db');
  Clear;
  Read;
end;

constructor TWorkList.Create(AnEmployee: TEmployee);
begin
  Inherited Create;
  FEmployee := AnEmployee;
  Criteria.ClearAll;
  Criteria.AddEqualTo('f_employee', FEmployee.OID.AsString);
  Criteria.AddOrderBy('f_date');

end;

{ TEmployeeList }

procedure TEmployeeList.SetShowDeleted(AValue: Boolean);
begin
  if FShowDeleted=AValue then Exit;
  FShowDeleted:=AValue;
  Read;
end;

procedure TEmployeeList.Read;
begin
  Clear;
  Criteria.ClearAll;
  Criteria.AddOrderBy('f_name');
  if not ShowDeleted then
  begin
    Criteria.AddEqualTo('f_isdeleted', False);
  end;
  inherited Read;
end;

procedure TEmployeeList.Save;
begin
  inherited Save;
end;

procedure TEmployeeList.UnMarkToDate(ADate: TDate);
var
  lItem: TEmployee;
begin
  for TtiObject(lItem) in Self do
  begin
    lItem.WorkList.UnmarkToDate(Adate);
  end;
end;

procedure TEmployeeList.MarkAllSubmitted;
var
  lItem: TEmployee;
begin
  for TtiObject(lItem) in Self do
  begin
    lItem.WorkList.MarkAllSubmitted;
  end;
end;

{ TEmployee }

procedure TEmployee.SetName(AValue: String);
begin
  if FName=AValue then Exit;
  FName:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

function Pad(S: String; PadChar: Char; Count: Integer): String;
var
  i: SizeInt;
begin
  REsult := s;
  for i := Length(S) to Count do
  begin
    Result += PadChar;
  end;
end;

procedure TEmployee.GenerateDetails(ALines: TStrings; var AFirst: TDateTime;
  var ALast: TDateTime; AIncludeSubmitted: Boolean; AsHTML: Boolean);
var
  i: Integer;
  lEntry: TWorkEntry;
  lCount: Integer = 0;
  lCurrentDay: TDate = 0;
  lAvg: Double;
begin
  if AsHTML then
  begin
    ALines.Add('<h2>Details for <i>%s</i></h2>' , [Name]);
    if WorkList.Count = 0 then
    begin
      ALines.Add('<p>No time</p>')
    end
    else
    begin
      ALines.Add('<table>');
      ALines.Add('<tr>');
      ALines.Add('<th>Date</th>');
      ALines.Add('<th>Day</th>');
      ALines.Add('<th>Start</th>');
      ALines.Add('<th>End</th>');
      ALines.Add('<th>Hours</th>');
      ALines.Add('</tr>');
    end;
  end
  else
  begin
    ALines.add('## Details for %s', [Name]);
    if  WorkList.Count = 0 then
      ALines.Add('- No time')
    else
    begin
      ALines.Add('| %s | %s | %s | %s | %s |', [Pad('Date'), Pad('Day'), Pad('Start'), Pad('End'), Pad('Hours')]);
      ALines.Add('| %s | %s | %s | %s | %s |', [Pad('', '-'), Pad('', '-'),Pad('', '-'), Pad('', '-'), Pad('', '-')]);
    end;
  end;

  for i := 0 to WorkList.Count-1 do
  begin
    lEntry := WorkList.Items[i];
    if lEntry.Submitted and not AIncludeSubmitted then
      Continue;

    if lCurrentDay <> lEntry.Date then
    begin
      Inc(lCount);
      lCurrentDay := lEntry.Date;
    end;
    if (AFirst = 0.0)
    or ((lEntry.Date < AFirst) and (lEntry.Date <> 0)) then
    begin
      AFirst:=lEntry.Date;
    end;
    if lEntry.Date > ALast then
      ALast:=lEntry.Date;
    if AsHTML then
    begin
      ALines.Add('<tr>');
      ALines.Add('<td>%s</td>', [DateToStr(lEntry.Date)]);
      ALines.Add('<td>%s</td>', [LongDayNames[DayOfWeek(lEntry.Date)]]);
      ALines.Add('<td>%s</td>', [FormatDateTime(DefaultFormatSettings.ShortTimeFormat, lEntry.StartTime)]);
      ALines.Add('<td>%s</td>', [FormatDateTime(DefaultFormatSettings.ShortTimeFormat, lEntry.EndTime)]);
      ALines.Add('<td>%s</td>', [FormatFloat('0.00', lEntry.Hours)]);
      ALines.Add('</tr>');
    end
    else
    begin
      ALines.Add('| %s | %s | %s | %s | %s |', [
                     Pad(DateToStr(lEntry.Date)),
                     Pad(LongDayNames[DayOfWeek(lEntry.Date)]),
                     Pad(FormatDateTime(DefaultFormatSettings.ShortTimeFormat, lEntry.StartTime)),
                     Pad(FormatDateTime(DefaultFormatSettings.ShortTimeFormat, lEntry.EndTime)),
                     Pad(FormatFloat('0.00', lEntry.Hours))
                     ]);
    end;
  end;
  lAvg := WorkList.Hours(AIncludeSubmitted);
  if lAvg <> 0.0 then
    lAvg := lAvg / lCount;
  if AsHTML then
  begin
    ALines.Add('</table>');
    ALines.Add('<p>Average Hours Per Day: <b>%.2f</b></p>', [lAvg]);
    ALines.Add('<p>Total Hours: <b>%.2f</b></p>', [WorkList.Hours(AIncludeSubmitted)]);
  end
  else begin
    ALines.add('');
    ALines.add('Average Hours Per Day: **%.2f**', [lAvg]);
    ALines.add('Total Hours: **%.2f**', [WorkList.Hours(AIncludeSubmitted)]);
  end;
end;

destructor TEmployee.Destroy;
begin
  if Assigned(FWorkList) then
  begin
    FWorkList.Save;
    FWorkList.Free;
  end;
  inherited Destroy;
end;

procedure TEmployee.SetActive(AValue: Boolean);
begin
  if FActive=AValue then Exit;
  FActive:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TEmployee.SetIsDeleted(AValue: Boolean);
begin
  if FIsDeleted=AValue then Exit;
  FIsDeleted:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

class function TEmployee.GetTable(AVersion: Integer; AIgnoreBefore: Integer
  ): TtiDBMetaDataTable;
begin
  Result := TtiDBMetaDataTable.Create;
  Result.Name:='employees';
  if (AVersion >=1) and (AIgnoreBefore <1) then
  begin
    Result.AddInstance('f_oid', qfkString, 36);
    Result.AddInstance('f_name', qfkLongString);
    Result.AddInstance('f_active', qfkLogical);
  end;
  if (AVersion >=2) and (AIgnoreBefore <2) then
  begin
    Result.AddInstance('f_isdeleted', qfkLogical);
  end;
end;

class procedure TEmployee.RegisterMappings;
begin
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'oid', 'f_oid', [pktDB]);
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'name', 'f_name');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'active', 'f_active');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'isdeleted', 'f_isdeleted');
  GTIOPFManager.ClassDBMappingMgr.RegisterCollection(TEmployeeList, Self);
end;

class function TEmployee.GetTableVersion: Integer;
begin
  Result := cVersion;
end;

class function TEmployee.GetTableName: String;
begin
  Result := cTableName;
end;

function TEmployee.GetWorkList: TWorkList;
begin
  if FWorkList = nil then
  begin
    FWorkList := TWorkList.Create(Self);
  end;
  FWorkList.Read;
  REsult := FWorkList;
end;

{ TWorkEntry }

procedure TWorkEntry.SetEndTime(AValue: TTime);
begin
  if FEndTime=AValue then Exit;
  FEndTime:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TWorkEntry.SetEmployee(AValue: TEmployee);
begin
  if FEmployee=AValue then Exit;
  FEmployee:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

class function TWorkEntry.GetTable(AVersion: Integer; AIgnoreBefore: Integer
  ): TtiDBMetaDataTable;
begin
  Result := TtiDBMetaDataTable.Create;
  Result.Name:='workentries';
  if (AVersion >=1) and (AIgnoreBefore <1) then
  begin
    Result.AddInstance('f_oid', qfkString, 36);
    Result.AddInstance('f_employee', qfkString, 36);
    Result.AddInstance('f_starttime', qfkDateTime);
    Result.AddInstance('f_endtime', qfkDateTime);
  end;
  if (AVersion >=2) and (AIgnoreBefore <2) then
  begin
    Result.AddInstance('f_date', qfkDateTime);
  end;
  if (AVersion >=3) and (AIgnoreBefore <3) then
  begin
    Result.AddInstance('f_submitted', qfkLogical);
  end;
end;

class procedure TWorkEntry.RegisterMappings;
begin
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'oid', 'f_oid', [pktDB]);
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'employeeoid', 'f_employee');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'starttime', 'f_starttime');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'endtime', 'f_endtime');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'date', 'f_date');
  GTIOPFManager.ClassDBMappingMgr.RegisterMapping(Self, cTableName, 'submitted', 'f_submitted');
  GTIOPFManager.ClassDBMappingMgr.RegisterCollection(TWorkList, TWorkEntry);
end;

class function TWorkEntry.GetTableVersion: Integer;
begin
  Result := cVersion;
end;

class function TWorkEntry.GetTableName: String;
begin
  Result := cTableName;
end;

function TWorkEntry.GetEmployeeOID: String;
begin
  Result := Employee.OID.AsString;
end;

function TWorkEntry.GetHours: Double;
var
  lStart, lEnd: TDateTime;
begin
  lStart := StartTime;
  lEnd := EndTime;

  if lEnd < lStart then
    lEnd := IncDay(lEnd);

  Result := (DateTimeToTimeStamp(lEnd-lStart).Time div 36000) / 100;

end;

procedure TWorkEntry.SetDate(AValue: TDateTime);
begin
  if FDate=AValue then Exit;
  FDate:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TWorkEntry.SetEmployeeOID(AValue: String);
begin
  FEmployee := GEmployees.Find(AValue);
end;

procedure TWorkEntry.SetStartTime(AValue: TTime);
begin
  if FStartTime=AValue then Exit;
  FStartTime:=AValue;NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TWorkEntry.SetSubmitted(AValue: Boolean);
begin
  if FSubmitted=AValue then Exit;
  FSubmitted:=AValue;
  NotifyObservers();
  if ObjectState <> posCreate then ObjectState:=posUpdate;
end;

procedure TWorkEntry.Save;
begin
  inherited Save;
end;

procedure CheckCreateDatabase;
begin
  GDBTables := TTableVersionList.Create;
  GDBTables.CreateTables;
  GDBTables.CheckUpgrade;
end;

procedure RegisterLog;
begin
  GLog.RegisterLog(TtiLogToConsole);
  GLog.LogLevel:=llVerbose;
end;

procedure RegisterMappings;
begin
  TTableVersionEntry.RegisterMappings;
  TEmployee.RegisterMappings;
  TWorkEntry.RegisterMappings;
  TReportItem.RegisterMappings;
end;

procedure ConnectDatabase;
var
  lDir: String;
begin
  GTIOPFManager.DefaultPersistenceLayerName := cTIPersistSqldbSQLite3;

  lDir := IncludeTrailingPathDelimiter(GetAppConfigDir(False));
  if not DirectoryExists(lDir) then
    ForceDirectories(lDir);

  GTIOPFManager.ConnectDatabase('db', ':'+GetSaveFile, '', '', '', cTIPersistSqldbSQLite3);
    CheckCreateDatabase;
end;

initialization
  //RegisterLog;
  RegisterMappings;
  ConnectDatabase;

  RegisterFallBackMediators;
  RegisterFallBackListMediators;
finalization
  GEmployees.Save;
end.

