{ <Frame for employees to add to the tabsheets dynamically>

  Copyright (C) <2021> <Andrew Haines> <andrewd207@aol.com>

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}
unit frame_Worktimes;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, StdCtrls, EditBtn, Grids, bom_objects, tiListMediators, tiObject,
  tiBaseMediator;

type

  { TWorktimesframe }

  TWorktimesframe = class(TFrame)
    AddButton: TButton;
    ShowSubmittedCheckBox: TCheckBox;
    DeleteButton: TButton;
    DateEdit1: TDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    StartTimeEdit: TTimeEdit;
    EndTimeEdit: TTimeEdit;
    StringGrid1: TStringGrid;
    procedure AddButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure FrameEnter(Sender: TObject);
    procedure ShowSubmittedCheckBoxChange(Sender: TObject);
    procedure StartTimeEditChange(Sender: TObject);
    procedure StringGrid1CheckboxToggled(sender: TObject; aCol, aRow: Integer;
      aState: TCheckboxState);
    procedure StringGrid1EditingDone(Sender: TObject);
    procedure StringGrid1SelectEditor(Sender: TObject; aCol, aRow: Integer;
      var Editor: TWinControl);
    procedure StringGrid1ValidateEntry(sender: TObject; aCol, aRow: Integer;
      const OldValue: string; var NewValue: String);
  private
    FDateEditor: TDateEdit;
    FTimeEditor: TTimeEdit;
    FMediator: TtiStringGridMediatorView;
    procedure EditorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditorKeyPress(Sender: TObject; var Key: char);
    procedure EditorKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure HandleBeforeSetupField(AObject: TtiObject;
      const AFieldName: string; var AValue: string);
    function Item: TEmployee;
  public
    procedure SetupMediator;
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;

  end;

implementation
uses
  DateUtils, tiLog, LCLType, Dialogs;

{$R *.lfm}

const
  cFields: array[Boolean] of String = (
           'Date(130);Hours(70);StartTime(150);EndTime(150)',
           'Submitted(100);Date(130);Hours(70);StartTime(150);EndTime(150)');


{ TWorktimesframe }

procedure TWorktimesframe.StartTimeEditChange(Sender: TObject);
begin
  GLog.Log(FormatDateTime('YY-MM-DD hh:mm', StartTimeEdit.Time+Today));
end;

procedure TWorktimesframe.StringGrid1CheckboxToggled(sender: TObject; aCol,
  aRow: Integer; aState: TCheckboxState);
var
  lEntry: TWorkEntry;
begin
  lEntry := TWorkEntry(FMediator.GetObjectFromRow(aRow));
  lEntry.Submitted:= aState = TCheckBoxState.cbChecked;
  lEntry.Save;
end;

procedure TWorktimesframe.StringGrid1EditingDone(Sender: TObject);
begin

end;

procedure TWorktimesframe.StringGrid1SelectEditor(Sender: TObject; aCol,
  aRow: Integer; var Editor: TWinControl);
var
  lCell: String;
  lItem: TWorkEntry;
begin
  if StringGrid1.Columns.Count=0 then
    Exit;
  lCell := StringGrid1.Columns[aCol].Title.Caption;
  lItem := TWorkEntry(FMediator.GetObjectFromRow(aRow));
  case  lCell of
    'Start', 'StartTime',
    'End', 'EndTime':
      begin
        Editor := FTimeEditor;
        Editor.BoundsRect := StringGrid1.CellRect(aCol,aRow);
        if lCell[1] = 'S' then
          TTimeEdit(Editor).Time:=lItem.StartTime
        else
          TTimeEdit(Editor).Time:=lItem.EndTime;
        Editor.BorderWidth:=0;
      end;
    'Date':
      begin
        Editor := FDateEditor;
        Editor.BoundsRect := StringGrid1.CellRect(aCol,aRow);
        TDateEdit(Editor).Date:=lItem.Date;
      end;
  end;
end;

procedure TWorktimesframe.StringGrid1ValidateEntry(sender: TObject; aCol,
  aRow: Integer; const OldValue: string; var NewValue: String);
var
  lObject: TtiObject;
  lProp: TtiMediatorFieldInfo;
  lValue: TDateTime;
begin
  NewValue:=StringGrid1.Editor.Caption;

  GLog.Log(Format('Edited %d:%d = %s [%s]', [acol,arow,NewValue, StringGrid1.Editor.Caption]));
  lObject := FMediator.GetObjectFromRow(aRow);
  lProp := FMediator.FieldsInfo.FieldInfo[aCol];
  case lProp.PropName of
    'Date' :
      begin
        lValue:= StrToDate(NewValue,'YYYY-MM-DD');
        lObject.PropValue[lProp.PropName] := lValue;
        TWorkEntry(lObject).save;
      end;
    'StartTime', 'EndTime':
      begin
        lValue:= StrToTime(NewValue);
        lObject.PropValue[lProp.PropName] := lValue;
        TWorkEntry(lObject).save;
      end
  else
    NewValue:=OldValue;

  end;



end;

procedure TWorktimesframe.SetupMediator;
begin
  if FMediator <> nil then
  exit;
  FMediator := TtiStringGridMediatorView.Create;
  FMediator.FieldName:=cFields[ShowSubmittedCheckBox.Checked];
  FMediator.OnBeforeSetupField:=@HandleBeforeSetupField;
  FMediator.Subject:=Item.WorkList;
  Item.WorkList.ShowSubmitted:=ShowSubmittedCheckBox.Checked;
  FMediator.SetView(StringGrid1);
  FMediator.Active:=True;

  StartTimeEdit.SimpleLayout:=true;
end;

constructor TWorktimesframe.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FDateEditor := TDateEdit.Create(Self);
  FDateEditor.DateFormat:='YYYY-MM-DD';
  FDateEditor.OnKeyDown:=@EditorKeyDown;
  FDateEditor.OnKeyUp:=@EditorKeyUp;
  FDateEditor.OnKeyPress:=@EditorKeyPress;

  FTimeEditor := TTimeEdit.Create(Self);
  FTimeEditor.OnKeyDown:=@EditorKeyDown;
  FTimeEditor.OnKeyUp:=@EditorKeyUp;
  FTimeEditor.OnKeyPress:=@EditorKeyPress;
end;

destructor TWorktimesframe.Destroy;
begin
  if Assigned(FMediator) then
  begin
    FMediator.Active:=False;
    FMediator.Free;
  end;

  inherited Destroy;
end;

function TWorktimesframe.Item: TEmployee;
begin
  Result := TEmployee(Pointer(Tag));
end;

procedure TWorktimesframe.HandleBeforeSetupField(AObject: TtiObject;
  const AFieldName: string; var AValue: string);
var
  lItem: TWorkEntry absolute AObject;
begin

  if AFieldName = 'Date' then
    AValue:=FormatDateTime('YYYY-MM-DD',lItem.Date);
  if AFieldName = 'StartTime' then
    AValue:=FormatDateTime(DefaultFormatSettings.ShortTimeFormat,lItem.StartTime);
  if AFieldName = 'EndTime' then
    AValue:=FormatDateTime(DefaultFormatSettings.ShortTimeFormat,lItem.EndTime);
end;

procedure TWorktimesframe.EditorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  StringGrid1.EditorKeyDown(Sender,key,Shift);
end;

procedure TWorktimesframe.EditorKeyPress(Sender: TObject; var Key: char);
begin
  StringGrid1.EditorKeyPress(Sender,key);
end;

procedure TWorktimesframe.EditorKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  StringGrid1.EditorKeyUp(Sender,key,Shift);
end;

procedure TWorktimesframe.AddButtonClick(Sender: TObject);
var
  lWorkTime: TWorkEntry;
begin
  StartTimeEdit.SetFocus;
  lWorkTime := TWorkEntry.CreateNew();
  lWorkTime.Employee := Item;
  lWorkTime.StartTime:=StartTimeEdit.Time;
  lWorkTime.EndTime:=EndTimeEdit.Time;
  lWorkTime.Date:=DateEdit1.Date;
  lWorkTime.save;
  Item.WorkList.Add(lWorkTime);
end;

procedure TWorktimesframe.DeleteButtonClick(Sender: TObject);
var
  lObject: TWorkEntry;
begin
  if StringGrid1.SelectedRangeCount > 0 then
  begin
    if QuestionDlg('Delete selected', 'Are you sure you want to delete the selected entry? This cannot be undone!',
       mtConfirmation,[mrYes,'Yes', mrCancel,'Cancel'],0) = mrYes then
    begin
      lObject := TWorkEntry(FMediator.GetObjectFromRow(StringGrid1.Selection.Top));
      if Assigned(lObject) then
      begin
        lObject.Deleted:=True;
        Item.WorkList.Save;
        Item.WorkList.Remove(lObject);
      end;
    end;
  end;
end;

procedure TWorktimesframe.FrameEnter(Sender: TObject);
var
  lLast: TWorkEntry;
begin
  if (DateEdit1.Date <> Today) then
  begin
    lLast := Item.WorkList.Last;
    if lLast = nil then
      DateEdit1.Date:=Today
    else
    begin
      DateEdit1.Date:=IncDay(DateOf(lLast.Date));
      StartTimeEdit.Time:=lLast.StartTime;
      EndTimeEdit.Time:=lLast.EndTime;
    end;
  end;

end;

procedure TWorktimesframe.ShowSubmittedCheckBoxChange(Sender: TObject);
begin
  Item.WorkList.Clear;
  FMediator.Active:=False;
  Item.WorkList.ShowSubmitted:=ShowSubmittedCheckBox.Checked;
  FMediator.FieldName:=cFields[ShowSubmittedCheckBox.Checked];
  FMediator.Active:=True;

end;

initialization
DefaultFormatSettings.ShortTimeFormat := 'h:nn AM/PM';
DefaultFormatSettings.ShortDateFormat:= 'YYYY-MM-DD';
DefaultFormatSettings.LongDateFormat:= 'mmmm" "dd", "yyyy';

end.

