{ <Frame for employees to add to the tabsheets dynamically>

  Copyright (C) <2021> <Andrew Haines> <andrewd207@aol.com>

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}
unit frame_employee;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ComCtrls, StdCtrls, Grids, tiBaseMediator,
  tiObject, bom_objects, tiListMediators;


type

  { TFrame1 }

  TFrame1 = class(TFrame)
    AddButton: TButton;
    EmployeeList: TStringGrid;
    RemoveButton: TButton;
    procedure AddButtonClick(Sender: TObject);
    procedure EmployeeListCheckboxToggled(sender: TObject; aCol, aRow: Integer;
      aState: TCheckboxState);
    procedure EmployeeListEditingDone(Sender: TObject);
    procedure EmployeeListSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure EmployeeListValidateEntry(sender: TObject; aCol, aRow: Integer;
      const OldValue: string; var NewValue: String);
    procedure RemoveButtonClick(Sender: TObject);
  private
    FMediator: TtiStringGridMediatorView;
  public
    procedure InitMediator;
    destructor Destroy; override;
  end;

implementation
{$R *.lfm}

uses
  tiLog, Dialogs;


{ TFrame1 }

procedure TFrame1.AddButtonClick(Sender: TObject);
var
  lEmployee: TEmployee;
begin
  lEmployee := TEmployee.CreateNew();
  lEmployee.Name := 'Unnamed';
  lEmployee.Active:=True;
  GEmployees.Add(lEmployee);
end;

procedure TFrame1.EmployeeListCheckboxToggled(sender: TObject; aCol,
  aRow: Integer; aState: TCheckboxState);
begin
  GEmployees.Items[aRow-1].Active:=aState = cbChecked;
end;

procedure TFrame1.EmployeeListEditingDone(Sender: TObject);
begin
  GLog.Log(Format('Editingdone',[]));
end;

procedure TFrame1.EmployeeListSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
begin
  GLog.Log(Format('SetText %d:%d', [acol,arow]));
end;

procedure TFrame1.EmployeeListValidateEntry(sender: TObject; aCol,
  aRow: Integer; const OldValue: string; var NewValue: String);
var
  lObject: TtiObject;
  lProp: TtiMediatorFieldInfo;
begin
  GLog.Log(Format('Edited %d:%d', [acol,arow]));
  lObject := FMediator.GetObjectFromRow(aRow);
  lProp := FMediator.FieldsInfo.FieldInfo[aCol];
  lObject.PropValue[lProp.PropName] := NewValue;
end;

procedure TFrame1.RemoveButtonClick(Sender: TObject);
var
  lItem: TEmployee;
begin
  if EmployeeList.SelectedRangeCount = 0 then
    Exit;

  lItem := TEmployee(FMediator.GetObjectFromRow(EmployeeList.SelectedRange[0].Top));

  if QuestionDlg('Remove User', Format('Are you sure you wish to permanantly remove the user %s',[lItem.Name]),mtConfirmation,[mrYes, 'Yes', mrNo, 'No'],'') = mrYes then
  begin
    GLog.Log(Format('Row = %d',[EmployeeList.SelectedRange[0].Top]));
    lItem.IsDeleted:=True;
    GEmployees.Save;
    GEmployees.Remove(lItem);
  end;
end;

procedure TFrame1.InitMediator;
begin
  if Assigned(FMediator) then
    Exit;
  FMediator := TtiStringGridMediatorView.Create;
  FMediator.Subject := GEmployees;
  FMediator.SetView(EmployeeList);
  FMediator.FieldName:='Active;Name';
  FMediator.Active:=True;
end;

destructor TFrame1.Destroy;
begin
  FMediator.Active:=False;
  FMediator.Free;
  inherited Destroy;
end;

end.

